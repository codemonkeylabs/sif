#ifndef _VIF_H_
#define _VIF_H_

#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <iostream>
#include <system_error>

class VIF {
  int fd_m;
  public:
    VIF() : fd_m(socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) {
        if (fd_m < 0) {
            throw std::system_error(errno, std::iostream_category(), "Failed to create VIF socket");
        }
    }
    ~VIF() {
        if (fd_m > 0) {
            close(fd_m);
        }
    }
    ssize_t tx(uint8_t* data, size_t data_len, sockaddr* addr, socklen_t addr_len) {
        return sendto(fd_m, data, data_len, 0, addr, addr_len);
    }
};
#endif