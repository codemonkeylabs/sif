include $(TOP)/build/header.mk

targets_$(d)    := sif

sif_sources_$(d)     := huvud.cpp nfq.cpp l3.cpp l4.cpp sif.cpp
sif_target_dir_$(d)  := bin
sif_cxx_flags_$(d)   := -O2
sif_ld_flags_$(d)    := -lnetfilter_queue -lnfnetlink

include $(TOP)/build/footer.mk
