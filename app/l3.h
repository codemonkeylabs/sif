/*
 * Heimdall L3 specific definitions
 *
 * Copyright (C) 2018 Erick Gonzalez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __HEIMDALL_L3_H__
#define __HEIMDALL_L3_H__

#include "huvud.h"

extern "C" {
    int       parse_l3(uint32_t id, uint8_t* data, uint32_t len, packet_t* pkt_p);
    verdict_t handle_l3_packet(huvud_queue_t* hq_p, uint32_t id, uint8_t* data, uint32_t len);
}

#endif
