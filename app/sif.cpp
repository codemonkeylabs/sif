#include <iostream>
#include "switch.h"
#include "huvud.h"

extern "C" {
    static const char* SIF_COMPONENT = "SIF";
    void log_fn(uint8_t level, const char* component, char* msg) {
        fprintf(stderr, "%s\n", msg);
    }

    verdict_t analyze_fn(void* q_p, packet_t* pkt_p) {
        Switch* switch_p = (typeof switch_p) q_p;
        switch_p->rx(pkt_p);
        return DROP_PACKET;
    }
}

int main(int n, const char* const argv[])
{
    huvud_queue_t* queue_p = huvud_init(0, SIF_COMPONENT, NULL, NULL, analyze_fn, log_fn);
    queue_p->q_p = new Switch();
    huvud_run_queue(queue_p);
    huvud_destroy(queue_p);
}

