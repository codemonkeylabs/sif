#ifndef _SWITCH_H_
#define _SWITCH_H_

#include "vif.h"
#include "huvud.h"

class Switch {
    VIF* vif_mp;
  public:
    Switch() : vif_mp( new VIF() ) {

    }
    ~Switch() {
        delete vif_mp;
    }
    void rx(packet_t* pkt_p) {
#ifdef __DEBUG__
        char buf[4096];
        hexdump(buf, sizeof(buf), pkt_p->pkt_start, pkt_p->pkt_len);
        fprintf(stderr, "switch rx packet:\n%s", buf);
#endif
        sockaddr_in saddr;
        saddr.sin_family      = AF_INET;
        saddr.sin_port        = 0;
        saddr.sin_addr.s_addr = pkt_p->dst_addr[0]; // IPV4 only for this quick test
        vif_mp->tx(pkt_p->pkt_start, pkt_p->pkt_len, (sockaddr*)&saddr, sizeof(saddr));
    }
};

#endif