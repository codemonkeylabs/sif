# Build instructions (Ubuntu Linux)

* `sudo apt-get install build-essential libnfnetlink-dev libnetfilter-queue-dev`
* `make`

# [Running]()

* Add an IPTables rule to redirect desired traffic to NFQ 0. For example: \
  `sudo iptables -t raw -A PREROUTING -s 10.10.0.0/16 -j NFQUEUE --queue-num 0`

The application reads packets from NFQ 0 and puts injects them back into the network via raw socket writes. That's all there is to it really.
